--
-- Populate the database
--
-- 1. Insert the programme in to the database.


-- 2. Insert the modules in to the database.


--
-- Import data into the database
-- 
-- 1. Copy the lessons from the temporary table to the lessons table, creation relations with the modules that they belong to.


--
-- Extend the database
--
-- 1. All the queries you executed to get the grades properly saved in the proper tables in the database.



--
-- Viewing student progress
--
-- 1. A view that display the student's name, the total amount of credits it has collected and the percentage of the collected credits compared to the total amount of credits of the programme.


-- 
-- One more thing...
-- There is one part of the database that can still be normalized. Can you spot it? If so update the database accordingly.
--
-- 1. Elaborate what part of the data can still be normalized and how you have updated the database.