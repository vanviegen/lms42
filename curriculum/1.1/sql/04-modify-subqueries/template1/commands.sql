-- Insert data into the database: 
-- 1. Insert a new category in the database called 'Millennium'.


-- 2. Insert a new actor in the dabase with the name Matthew McConaughey.


-- 3. Insert a new movie into the database with name Interstellar, release year 2014, length 169 and rating PG-13. The rental duration 3 weeks, at the cost of 4.99 with a replacement value of 20.99. The movie contains the following special features "{Trailers,"Deleted Scenes"}" and description: "A team of explorers travel through a wormhole in space in an attempt to ensure humanity's survival.". For the fulltext supply an empty string.


-- 4. Matthew McConaughey is an actor in Interstellar. Insert this link into the database.  


-- 5. Add the film Interstellar in the inventory of the store located on '28 MySQL Boulevard'.


-- 6. Insert a new staff member in the database with name Bob Alisson and email bob.alisson@saxion.nl, who lives at 'Tromplaan 28' in Enschede, the Netherlands. Bob works in the store on 'MySQL Boulevard' and is the active manager of the (new) store located at 'Van Galenstraat 19' in Enschede in the Netherlands (no district or zipcode). Insert the store as well. 


-- Update data in the database:
-- 1. Update staff member Jon Stephens email adress to jon.stephens@saxion.nl.


-- 2. All movies with id higher than 969 (excluding this id) have been moved to the new store at 'Van galenstraat 19'. Update the inventory accordingly.


-- 3. Update all customers with first or last name 'Morris' so that they are a customer of the store at 'Van galenstraat 19'. 


-- 4. Update all payments conducted on 14th of Februari 2007 so that everyone has a 50 cent discount.


-- Remove data from the database:
-- 1. Remove the category new for all the films that have it. Note that you should not remove the film but simply the catergory for the film.


-- 2. Remove the category with the name 'New'.


-- 3. Delete all rentals that have been rented out on 30th of May on 2005. 


-- 4. Delete all the movies with id 1000 from the inventory.


-- Subqueries:
-- 1. Show all the customers that have the same first name as any of the staff members. Display the customer's first and last name along with their address and country. Sort the list by first name (ascending).


-- 2. Delete all films from the inventory where the film has 'bug' in the title.


-- 3. Find all films with a duration longer than the average film duration. Display the title and length of the film. Sort by duration (descending).


-- 4. Find all films that have been released in the same year as the latest 'G' rated film where 'Sean Guiness' was an actor in. Display the title, release year and rental rate. Sort the results by rental rate from most expensive to the cheapest.


-- 5. Find the movie buffs (the binge watchers). Display the first and last name of all the customers that have rented more films than average. Also include the number of rentals they have and sort the results from customer with most rented films to least rented.


-- 6. Relabel the millennium movies. Add the category 'Millennium' (in film_category) to all the films that have no category and have a release year greater than 1999. 



-- Views
-- 1. Create a view of ordered payments. The view should be named 'v_ordered_payments's and should return the first and last name of the customer who made the payment and staff who processed the payment. In addition the amount and payment date should be returned. Show the results by querying the view for all payments done by 'Jon Stephens'.


-- 2. Create a view (v_daily_income) for the sum of all payments received in a day. Display the year, month, day and the total amount in payments received on that day. Query all the entries from the view.


-- 3. Create a view (v_outstanding_rental) that shows the films that have been rented but not yet returned. Display the rental date, film title and rental duration. Also include the customer's first name, last name and id. Query the view so that you have a list of outstanding rental with a duration of more than 4 days. Display the the rental date, film title, the customer's first name, last name and email address.


-- 4. Create a view (`v_unpaid_rentals`) that returns the number of unpaid rentals for each day. The results should be ordered by date. Use this view to query the list of customers that have rented a film on the day with the most unpaid rentals.
