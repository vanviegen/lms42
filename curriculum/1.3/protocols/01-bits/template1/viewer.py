from bit_reader import BitReader
from PIL import Image
import sys
import time

def decode(data):
    """Decodes `data` (a `bytes` list) to a PIL Image object."""

    reader = BitReader(data)

    # TODO: check the b"Img42" magic marker

    # TODO: read `width` and `height`
    width = 0
    height = 0

    # TODO: fill the bytearray with pixels, top to bottom -> left to right -> red byte + green byte + blue byte.
    pixels = bytearray()

    return Image.frombuffer("RGB", (width,height), bytes(pixels))


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(f"Usage: {sys.argv[0]} <img42-file>")

    with open(sys.argv[1],'rb') as file:
        data = file.read()

    start_time = time.perf_counter()
    image = decode(data)
    end_time = time.perf_counter()

    print('Decompressed %d kb of img42 data into %d kb of pixel data in %.3fs' % (
        round(len(data)/1024),
        round(len(image.tobytes())/1024),
        end_time-start_time,)
    )

    image.show()
