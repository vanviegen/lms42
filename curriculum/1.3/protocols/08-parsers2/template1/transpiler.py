import sys
from lark import Lark, Transformer


def indent(code):
    """Returns a copy of the given code string, but indented with an (additional) 4 spaces
    before every line."""
    return "    " + str(code).replace("\n", "\n    ")


class FuncyTransformer(Transformer):
    # TODO!
    pass


# Read the grammar and create a parser from it
with open("language.lark") as file:
    parser = Lark(file.read())

# Read funcy source
with open(sys.argv[1]) as file:
    source = file.read()

# Parse funcy source
tree = parser.parse(source)
print("-"*20 + " AST " + "-"*20)
print('tree', tree.pretty())

# Transform into Python
python = FuncyTransformer().transform(tree)
print("\n" + "-"*20 + " Python " + "-"*20)
print(python)

# Run the python code
print("\n" + "-"*20 + " Run " + "-"*20)
eval(python)
