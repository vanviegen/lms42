from tree import *
import json
import sys

# The binary operators that should be supported. The first list has the lowest precedence,
# the last list has the highest precedence. All operators are left-associative.
OPERATORS = [
    ['==', '!=', '<=', '>=', '<', '>'],
    ['=', '+=', '-=', '*=', '/='],
    ['+', '-'],
    ['*', '/', '%'],
]


class Parser:
    def __init__(self, tokens):
        self.tokens = tokens
        self.current_token_pos = 0
        self.expected_tokens = [] # The list of things the current token did *not* match (used for error reporting).
    
    @property
    def token(self):
        """Get the current token. This is a @property, so use `self.token` instead of `self.token()`."""
        return self.tokens[self.current_token_pos]

    def next_token(self):
        """Progress to the next token."""
        self.expected_tokens = []
        self.current_token_pos += 1

    def match(self, text=None, type=None):
        """This is a utility function you may want to use instead of using `token` and `next_token()`
        directory. Give either a token text *or* a token type, it returns the current token *if* it
        matches the text or type, or None otherwise.
        In case of a match, next_token() is called (but the original token is returned).
        In case of no match, the text or type is added to `expected_tokens`, which helps `error` give
        a better message."""
        assert(not text or not type)
        if (text==None or text==self.token[1]) and (type==None or type==self.token[0]):
            token = self.token
            self.next_token()
            return token
        self.expected_tokens.append(text if text else f"<{type}>")
        return False

    def error(self):
        """Display an error message, indicating the line, column, text and type of the current token and 
        the list of tokens that were expected instead (from `self.expected_tokens`), and exits the program. 
        """
        print(f"Expected {' or '.join(self.expected_tokens)} at line {self.token[2]} column {self.token[3]}, but found [{self.token[0]}] '{self.token[1]}'.")
        sys.exit(1)

    def require(self, val):
        """A utility function that may be convenient instead of checking for errors manually. It
        exits with an error if `val` is `None` or `False`, or just returns `val` otherwise.
        """
        if val==False or val==None:
            self.error()
        return val

    def parse(self):
        """The main parse method. It returns a Program object, or exits with an error.
        It should be called only once on an instance."""
        block = self.parse_statements()
        # Make sure that after parsing all statements that we can, we have arrived at the
        # end-of-file marker. (If not, that would indicate code at the end.)
        self.require(self.match(type='eof'))
        return Program(block)

    def parse_statements(self):
        # """Parses a (possibly empty) list of statements, and returns it as a Block."""
        TODO()

    
    # TODO: quite a few more parse_... functions!
