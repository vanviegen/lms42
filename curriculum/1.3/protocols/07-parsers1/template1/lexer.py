# You should not need to change this file. (But feel free to do so anyway!)

import re

# The lexing regular expression. It matches everything, wrapping every
# matched token into a named capture group.
matcher = re.compile(rf'''
(?P<string>
    \" (?: \\ . | [^"] )* \" |
    \' (?: \\ . | [^'] )* \'
) |
(?P<comment>
    //.* |
    /\* [\w\W]*? \*/
) |
(?P<keyword>
    if | else | while | var
) \b |
(?P<identifier>
    [a-zA-Z_][a-zA-Z_0-9]*
) |
(?P<number>
    -? \d+ (?: \. \d*)?
) |
(?P<whitespace>
    \s+
) |
(?P<operator>
    [-+=/*%!&|<>]+
) |
(?P<delimiter>
    [(){{}};,]
) |
(?P<unknown>
    [\w\W] # Anything else. This should imply an error, the parser should never match this token.
)
''', re.MULTILINE | re.VERBOSE)


def lex(source):
    """Given a Javascript source string, returns a list of (type, text, line, column) tuples that
    represent the individual tokens."""
    tokens = []
    line = 1
    col = 1
    for match in matcher.finditer(source):
        text = match.group()
        tokens.append((match.lastgroup, text, line, col,))
        
        # Progress the line and column, based on the number of newlines and other
        # characters we have scanned.
        last_newline = text.rfind("\n")
        if last_newline:
            line += text.count("\n")
            col = len(text) - last_newline
        else:
            col += len(text)

    # Add an end-of-file marker. This makes parsing a bit easier.
    tokens.append(('eof', '', line, col,))
    return tokens
