Introduction: |
     The graduation portfolio consists of eight parts, corresponding with the eight *end qualifications* of the study programme. For each of these parts, the student must receive a passing grade (5.5 or higher) in order to graduate. The grades are decided upon by the examination committee (consisting of two teachers and usually one adviser representing the software industry) based on the portfolio and how the student defends it during the graduation session.
     
     A student is expected to *grow* her/his portfolio during the entire second year. The portfolio should generally consists of professional products created as part of the three internship and the two free projects, but professional products created in different contexts (such as a part-time job or a hobby project) may be admissable as well.

Graduation supervisor: |     
     At the start of the second year, one of the teachers will be appointed graduation supervisor for a student. He or she will coach the student with regard to the portfolio on a biweekly basis. One important aspect of the coaching is searching for opportunities to give substance to a portfolio part based on what the student is currently doing anyway. Another aspect is to provide the student with early feedback on how he/she currently stands with regard to graduation. The graduation supervisor will *green light* planning a graduation session once he/she is confident that the outcome would be satisfactory.

     In order to allow to supervisor to keep track on your progress, you should


1. Functional design:
     - 
          Create a functional design (of limited complexity), based on the wishes of an (internal) customer.

     - Deliverables: |
          Hand in the functional design for a project of your choice. We would recommend it be a (web)app with a graphical user interface. Although the exact contents of a functional design may differ depending on the project, we would generally expect to see:
          
          - An introduction and a problem statement.
          - A description of the process you used to analyse the requirements. Motivate your choices.
          - A prioritized list of SMART business requirements and/or user stories.
          - Wireframes of the most complex pages.
          - Basic graphical designs for a couple of these wireframes.

     - Grading: |
          Grading is based on the quality of your design, correcting for the complexity of the problem and the amount of independence with which you did the work.

          | Grade | Quality (completeness, clarity, accuracy) |
          | -- | -- |
          | 1 | There is nothing of any use. One would need to start over entirely. |
          | 5 | The FD has rather many large flaws. Even when in a rush, one would need to iterate on the FD before continuing development. |
          | 6 | The FD has some large flaws, but when in a rush it could be used as a starting point for creating a TD and an implementation. |
          | 8 | The FD has some shortcomings, but for the most parts provides enough guidance for creating a TD and an implementation. |
          | 10 | The FD is complete, clear and accurate. A TD and an implementation can be created without any further knowledge of the project. |

          | Multiplier | Complexity |
          | -- | -- |
          | 0.6 | About the complexity of a blog, including sign ups, managing posts and (unmoderated) user comments. |
          | 1.0 | About as complex as a web store, including sign ups, shopping basket, checkout, payment and product recommendations. |
          | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

          | Multiplier | Independence |
          | -- | -- |
          | 0.5 | The student contributed only about half of the work. |
          | 0.8 | Although the student did most of the work on the FD, it's creation was tightly directed by someone else. |
          | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
          | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |

2. Technical design:
     -  
          Create a technical design (of limited complexity), based on a (part of a) functional design.

     - Deliverables: |
          Hand in the technical design for a project of your choice. Although the contents of a technical design may differ depending on the project, we would generally expect to see:
          
          - An introduction.
          - A short description of the functionality of the system that needs to be build (or a reference to a relevant functional design).
          - A (high level) technical overview, including at least one UML diagram.
          - A description of design choices, including motivation of the choices made and alternatives that have been considered.
          - A domain model, consisting of (the most important) real world 'things' and their relations.
          - A test plan.

     - Grading: |
          Grading is based on the quality of your design, correcting for the complexity of the problem and the amount of independence with which you did the work.

          | Grade | Quality (completeness, clarity, accuracy) |
          | -- | -- |
          | 1 | There is nothing of any use. One would need to start over entirely. |
          | 5 | The TD has rather many large flaws. Even when in a rush, one would need to iterate on the TD before continuing development. |
          | 6 | The TD has some large flaws, but when in a rush it could be used as a starting point for creating an implementation. |
          | 8 | The TD has some shortcomings, but for the most parts provides enough guidance for creating an implementation. |
          | 10 | The TD is complete, clear and accurate. The TD has a clear link with the functional design and an implementation (based on the TD) can be made without any further enquiry. |

          | Multiplier | Complexity |
          | -- | -- |
          | 0.6 | About the complexity of a blog, including sign ups, managing posts and (unmoderated) user comments. |
          | 1.0 | About as complex as a web store, including sign ups, shopping basket, checkout, payment and product recommendations. |
          | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

          | Multiplier | Independence |
          | -- | -- |
          | 0.5 | The student contributed only about half of the work. |
          | 0.8 | Although the student did most of the work on the TD, it's creation was tightly directed by someone else. |
          | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
          | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |

3. Implementation:
     - 
          Implement new software or extend existing software, based on a technical design, with regard for the quality aspects reliability, readability, adaptability, maintainability, security and performance.
     
     - Deliverables: |
          Hand in a document describing two projects you have worked on, including the source code of the two projects (including version management). These can be green field projects or extensions made to existing applications (in this case you are allowed to submit a patch set). In the document you should describe the additions made to the code base. The submitted source code should have a minimum of 500 lines of code (excluding scaffolding or generated code).
     
          For each of the two projects we would generally expect to see the following:

               * A short description of the project.
               * A description of the functionality that you have added or problems you have solved. Screenshots/recordings very welcome!
               * The part of the technical design relevant for the supplied source code, if any.
               * The source code (or patch), including any documentation and testing.

     - Grading: |
          Grading is based on the quality of your implementations, with regard for quality aspects: reliability, readability, adaptability, maintainability, security and performance.
          
          | Grade | Code quality rubrics from assignments |
          | -- | -- | 
          | 1 | The code would have to be entirely rewritten in order to be accepted in a production environment.
          | 5 | Code would require major refactoring to be accepted in a production environment.
          | 6 | Code could be accepted in a production environment with some minor revision.
          | 8 | Code can be accepted in a production environment as-is.
          | 10 | Exceptionally well-structured, elegant and/or readable code, with a lot of attention to detail.

          | Multiplier | Complexity |
          | -- | -- |
          | 0.6 | About the complexity of a blog, including sign ups, managing posts and (unmoderated) user comments. |
          | 1.0 | About as complex as a web store, including sign ups, shopping basket, checkout, payment and product recommendations. |
          | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

          | Multiplier | Independence |
          | -- | -- |
          | 0.5 | The student contributed only about half of the work. |
          | 0.8 | Although the student did most of the work on the TD, it's creation was tightly directed by someone else. |
          | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
          | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |

4. Problem solving:
     - 
          Solve problems using a solid theoretical basis.
          
     - Deliverables: | 
          Hand in a document describing two (technically) challenging problems you have encountered and how you have solved them. 
          
          For each of the challenges, we would generally expect to see:
     
          - An introduction.
          - A problem statement.
          - A description of the process you used to analyse the problem.
          - References to resources you have consulted when solving the problem.
          - A concise description of the implemented solution and alternatives you have considered.
          - A critical (if possible numerically supported) analysis of your solution's fitness. For example in terms of time and/or space complexity, or in terms of meeting some specific goal.

     - Grading: |
          Grading is based on the quality of your solution, correcting for the complexity of the problem and the amount of independence with which you did the work.

          | Grade | Quality (completeness, clarity, accuracy) |
          | -- | -- |
          | 1 | The presented solutions do not solve the presented problem at all. |
          | 5 | The presented solutions are severely flawed. Little to no alternatives have been considered or an obvious alternative has been missed. |
          | 6 | The presented solutions has some large flaws, relevant alternatives have been considered but the derivation of the solution is flawed. |
          | 8 | The presented solutions has some shortcomings, but for the most parts provides enough guidance for creating a production grade implementation. |
          | 10 | The presented solutions complete, clear and accurate. An expert presented with the problem would consider similar alternatives and come to a similar conclusion. |

          | Multiplier | Complexity |
          | -- | -- |
          | 0.6 | A problem that can be solved without much of an algorithm, but by intelligently combining standard data structures. |
          | 1.0 | About as complex as the teleporting-maze assignment. |
          | 1.2 | Clearly more complex than the above example, or about as complex but in a problem domain that is less well understood. |

          | Multiplier | Independence |
          | -- | -- |
          | 0.5 | The student contributed only about half of the work. |
          | 0.8 | Although the student did most of the work analyzing the problems, the solution was tightly directed by someone else. |
          | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
          | 1.2 | The student worked very independently on topics no coworker or teacher would be able to provide assistance with. |


5. Technology skills:
     - 
          Hit the ground running with various commonly used programming techniques.
          
     - Deliverables: |
          Create a video presentation (of no more than 10 minutes) in which proudly demonstrate and tell about all the software you have created. This may include what you did in your internships, your free projects, or in any other context (except for the other *sd42* assignments).

          In case you are not the only author of a piece of software, make sure you point out very explicitly what part you contributed.

          The video should mostly be proof of the fact that you are able to produce significantly sized programs in a reasonable amount of time, in multiple different technological environments. It is up to you how many different projects you want to discuss in order to show this.

     - Grading: |
          Grading is based on the diversity, complexity and volume of your work.

          TODO: How do we let go of the time constraints posed by internships and free projects, to allow for part-time students?

          | Grade | Quality (diversity, complexity and volume) |
          | -- | -- |
          | 1 | Should not be possible (?) . |
          | 5 | The student shows he/she is a one trick pony. |
          | 6 | The student shows he/she can implement some (?) simple features in at least two types (frontend, backend, mobile) in a predefined time period (10 weeks). |
          | 8 | The student shows he/she can implement many (?) complex features in at least three types (frontend, backend, mobile) in a predefined time period (10 weeks). |
          | 10 | The student shows he/she has a broad skill set which is reflected by vast amount of work done in the internships and projects. The internships and projects use different techniques. |

          Alternative:
          
          | Grade | Diversity |
          | -- | -- |
          | 0 | In all work presented a similar application has been implemented in the same programming language. |
          | 1 | Presented work contains many similarities (at most two different sort of applications in two different programming languages). |
          | 2 | Presented work contains some similarities (at most three different sort of applications in three different programming languages) |
          | 3 | For every internship and project a different type of application has been implemented in a different programming language. |
          
          | Grade | Complexity |
          | -- | -- |
          | 0 | In all work presented the implemented features are trivial. |
          | 1 | Presented work rarely (at most two of the five assignments) consists of solving complex (technical) problems. |
          | 2 | Presented work mostly (at least three of the five assignments) consists of solving complex (technical) problems. |
          | 3 | Every internship and project involves solving complex (technical) problems. |
          
          | Grade | Volume |
          | -- | -- |
          | 0 | In the presented work very little (one or two features per internship/project) has been implemented. |
          | 1 | Presented work ?? . |
          | 2 | Presented work ?? . |
          | 3 | For every internship and project a feature-rich implementation has been delivered. |
          
          Final grade: sum grades diversity, complexity and volume + 1
          Requirement: No single grade is 0.

6. Self-directed learning:
     -
          Quickly and independently learn to use new programming techniques.
     
     - Deliverables: |
          - A journal for at least 100 working days of doing development work. For each day, the following questions should be answered:
                1. What have you accomplished today?
                2. What are the most important things you learned today?
                3. What are the most important questions you still have at the moment?
          - For each of three new skills/techniques (like a programming language, sophisticated library, framework, complex algorithm, complex tooling, etc) that you have had to learn 'on the job' (during internships, free projects, or some extracurricular activity):
               - A description of the context. What skill were you lacking, and what did you need it for?
               - An introduction to the (basics of the) of the skill in your own words in about 400 words. You may include short code examples, if that helps. The text should be targeted towards fellow students who completed the first year, but did not study this particular skill.
               - How you approached learning the skill. Ideally you would be able to refer to specific parts of your journal here.

     - Grading: |
          | Grade | Complexity/extensity |
          | -- | -- |
          | 1 | Nothing new. |
          | 3 | Light use of a relatively simple new library (like chart.js). |
          | 5 | Light use of a new programming language (like C), an elaborate new framework (like Django) or a complex library (like D3.js). Or extensive use of a simpler dependency. |
          | 7 | Extensive use of a new programming language *or* an elaborate new framework *or* a complex library. |
          | 9 | Extensive use of a new programming language *and* an elaborate framework/complex library on top of that. |

          | Multiplier | Novelty |
          | -- | -- |
          | 0.6 | The new technique is really similar to something in the curriculum. It's mostly just syntax and/or names that are different. Semantics are almost identical. |
          | 1.0 | The new technique is conceptually similar to something within the curriculum, but has many many small differences in semantics. (Like going from Python to Ruby.) |
          | 1.2 | The new technique is conceptually unlike anything in the curriculum. (Like going from Python to Clojure, or from SQL+REST to NoSQL+GraphDB.) |

          | Multiplier | Independence |
          | -- | -- |
          | 0.5 | The student contributed only about half of the work. |
          | 0.8 | Although the student learned the techniques a lot of guidance was needed. |
          | 1.0 | The student did the work mostly independently, only asking for help on specific topics, after having attempted to find the answers independently. | 
          | 1.2 | The student worked very independently on topics without the assistance of a coworker or teacher. |


7. DevOps: 
     -
          Automate software testing and deployment.
          
     - Deliverables: |
          Hand in a document in which you describe how you set up DevOps for a web application or an network-based API. This should include (most of) the following topics:
          
          - Automated (unit and/or end-to-end) testing.
          - Pre-commit quality checks.
          - Containerization.
          - Continuous integration.
          - (Continuous) deployment.

          TODO: Where should things like linting and test coverage be checked? Pre-commit, or in the CI? Do we need both?
          
          For each of these topics we'd like to see:

          - A description of how you set things up.
          - Source code where applicable, or perhaps a screenshot.
          - Your motivation for doing this. What benefits does it have for future development?

     - Grading: |
          (Beoordelen op basis van een paar eik-voorbeelden.)

          TODO!

          | Grade | Quality (completeness, clarity, accuracy) |
          | -- | -- |
          | 1 | No automation has been applied. |
          | 5 | The project contains automated unit- or end-to-end testing with limited test coverage (60%) (?). |
          | 6 | The project contains automated unit- or/and end-to-end testing with a proper test coverage (80%) (?).  |
          | 8 | The project has a branching strategy for separate test- and production build. For each build separate (automated) testing has been implemented. |
          | 10 | A full fledged CI/CD pipeline has been implemented containing automated testing and deployment using containers. |

8. Teamwork:
     -
          Effectively collaborate as part of a development team, using modern tools and methodologies.

     - Deliverables: |
          Hand in a document that reflects on how you have behaved as part of a software development team.

          The document should contain:

          - For three different companies (or independent teams with very different processes within a company):
               - For each of the roles within the company that a software developer would encounter in her/his work:
                    * Describe the role and its purpose within the company (for example developer, scrum master, tester, marketeer, supervisor, HR manager, UX designer, etc).
                    * Describe whether the role is full filled by a software engineer or someone with a different background (if so give a short description).
                    * Describe the purpose of your collaboration. Briefly describe how often you met and what you have done to make this collaboration as smooth as possible.
               - A description (about 800 words) of the development process, focussed on the most important things the company does to achieve a high development quality and velocity. Topics may include: all kinds of regular meetings, review procedures, collaboration tools, roles, etc. 
               - Your recommendations to the company on how the development process could perhaps be improved. Make sure your account for the inherent differences between companies.
               - A written response to your process description and recommendations from a senior developer working at the company, regarding the accuracy of your description and the value of your recommendations. (For at least two companies/teams.)
          - A table that clearly shows the most important differences and commonalities between the processes of the three companies.
          
          Your descriptions and recommendations can be based on your own experiences while doing an internship at (or working for) the company, or can be based on interviews with developers at the company.
   
     - Grading: |
          TODO: How to not make this incredibly vague. Idea: from the first few documents to be turned in, select a few exemplary examples, grade them holistically and use them for calibration.
          | Grade | Roles & process descriptions & table quality |
          | -- | -- |
          | 10 | The roles and process and all of the three companies is thoroughly and clearly described. Differences and commonalities are perfectly clear. |

          | Grade | Recommendation value |
          | -- | -- |
          | 10 | ??|

Grading:
- You will present and defend your portfolio during a public defence. During this defence, you will be questioned by the graduation committee, consisting of two examiners (teachers) and usually a business representative (a senior professional software developer).
- Each of the eight parts will be graded by the two examiners, with the business representative attending in an advisory capacity.
- If any of the partial grades is lower than 5.5, the lowest will be your final grade. If all grades are 5.5 or higher, the average will be your final grade.
