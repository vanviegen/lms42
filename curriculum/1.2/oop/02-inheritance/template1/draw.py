import pygame

MAX_WIDTH = 800
MAX_HEIGHT = 600
shape_color = (0,0,255)
background_color = (255,255,255)

def draw_shapes(shapes):
  screen = pygame.display.set_mode((MAX_WIDTH, MAX_HEIGHT))
  pygame.display.set_caption('Inheritance')
  screen.fill(background_color)

  # Draw stuff here..
  # For example a circle.
  pygame.draw.circle(screen, shape_color, (100,100), 15, 1)

  pygame.display.flip()
  running = True
  while running:
    for event in pygame.event.get():
      if event.type == pygame.QUIT:
        running = False
