# This is some test code for a primer on class and static methods.
class SingletonExchange():
    exchange_rate = 10
    __instance = None

    def get_instance():
      # TODO: 4. Implement the singleton pattern 
      # NOTE: that the current return statement is WRONG !!!
      return SingletonExchange()

class Exchange():
    exchange_rate = 1

def main():
    # Create two instances of Exchange.
    e1 = Exchange()
    e2 = Exchange()
    
    # Print their exchange rates. 
    print('--------------------')
    print(f"e1: {e1.exchange_rate}")
    print(f"e2: {e2.exchange_rate}")

    # TODO: 1. Implement a (class) method called 'update' in the Exchange class.

    # TODO: 2. Use the update method here to set the exchange rate to a value of 2.  

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"e1: {e1.exchange_rate}")
    print(f"e2: {e2.exchange_rate}")

    # Set the value of e1 to 3. 
    e1.exchange_rate = 3

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"e1: {e1.exchange_rate}")
    print(f"e2: {e2.exchange_rate}")

    # TODO: 3. Use the update method here to set the exchange rate to a value of 4.  

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"e1: {e1.exchange_rate}")
    print(f"e2: {e2.exchange_rate}")

    # TODO: 4. Make SingletonExchange a singleton (see above).  

    # Create 'two instances' of the SingletonExchange class. 
    se1 = SingletonExchange.get_instance()
    se2 = SingletonExchange.get_instance()

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"se1: {se1.exchange_rate}")
    print(f"se2: {se2.exchange_rate}")

    # TODO: 5.Implement a class method called 'update' in the SingletonExchange class.

    # TODO: 6. Use the update method here to set the exchange rate to a value of 11.  

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"se1: {se1.exchange_rate}")
    print(f"se2: {se2.exchange_rate}")

    # Set the value of se1 to 12. 
    se1.exchange_rate = 12

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"se1: {se1.exchange_rate}")
    print(f"se2: {se2.exchange_rate}")

    # TODO: 7. Use the update method here to set the exchange rate to a value of 13.  

    # Print their exchange rates again to see what happens.
    print('--------------------')
    print(f"se1: {se1.exchange_rate}")
    print(f"se2: {se2.exchange_rate}")

    # TODO: 8. Open analysis.txt and write down your analysis
    print('--------------------')
    print("End! When you are finished please write down your analysis in 'analysis.txt'.")
    print('--------------------')


if __name__ == "__main__":
    main()