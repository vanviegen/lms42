PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
CREATE TABLE quotes (id integer PRIMARY KEY, text text NOT NULL, attribution text, user_id int NOT NULL DEFAULT (0));
INSERT INTO quotes VALUES(1,'Common sense is the collection of prejudices acquired by age eighteen.','Albert Einstein',0);
INSERT INTO quotes VALUES(2,'Great minds discuss ideas; average minds discuss events; small minds discuss people.','Eleanor Roosevelt',0);
INSERT INTO quotes VALUES(3,'If you aren''t, at any given time, scandalized by code you wrote five or even three years ago, you''re not learning anywhere near enough.','Nick Black',0);
INSERT INTO quotes VALUES(4,'What one programmer can do in one month, two programmers can do in two months.','Fred Brooks',0);
INSERT INTO quotes VALUES(5,'Never trust a computer you can''t throw out a window.','Steve Wozniak',0);
INSERT INTO quotes VALUES(6,'Really, I''m not out to destroy Microsoft. That will just be a completely unintentional side effect.','Linus Torvalds',0);
INSERT INTO quotes VALUES(7,'I don''t want to rule the universe. I just think it could be more sensibly organised.','Eliezer Yudkowsky',0);
INSERT INTO quotes VALUES(8,'The things you own end up owning you.','Tyler Durden',0);
INSERT INTO quotes VALUES(9,'You complete me.','Jerry Maguire',0);
CREATE TABLE users (id integer PRIMARY KEY, name text NOT NULL UNIQUE, password text NOT NULL);
CREATE TABLE comments (id integer PRIMARY KEY, quote_id int NOT NULL, user_id int NOT NULL, time DATETIME NOT NULL DEFAULT (datetime()), text text NOT NULL);
CREATE UNIQUE INDEX quotesUniqueIndex ON quotes (text, attribution);
CREATE INDEX commentsCompundIndex ON comments (quote_id, id);
COMMIT;

