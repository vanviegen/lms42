# These should be all the imports you need:
from flask import Flask, render_template, request
from random import randint

app = Flask(__name__)
app.config['TEMPLATES_AUTO_RELOAD'] = True

# Run using `poetry install && poetry run flask run --reload`

with open('words.txt') as file:
    words = file.read().strip().split("\n")

def guess_to_hint(guess, secret):
    """Given a `guess` and a `secret` word as strings, it returns a list with one tuple for
    each letter in the guess, where the first item is the letter, and the second item is one
    of the strings `correct`, `wrong` or `misplaced`, describing what applies for that letter.
    """
    result = []
    for idx, letter in enumerate(guess):
        actual = secret[idx]
        if actual==letter:
            result.append((letter, 'correct'))
        elif letter in secret:
            result.append((letter, 'misplaced'))
        else:
            result.append((letter, 'wrong'))
    return result

@app.route("/")
def index():
    return "Hello web!"
