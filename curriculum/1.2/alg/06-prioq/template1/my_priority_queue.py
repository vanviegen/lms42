import priority_queue_tests

class MyPriorityQueue():

    data = [] # TODO: you may want to use a different data structure here

    def add(self, value):
        """Add `value` to the priority queue."""
        # TODO

    def fetch_smallest(self):
        """Find the smallest value in the priority queue, remove it from the
        queue and return it."""
        # TODO

    def is_empty(self):
        """Returns `True` is the queue is empty, and `False` otherwise."""
        # TODO

if __name__ == '__main__':
    priority_queue_tests.run_all_tests(MyPriorityQueue)
