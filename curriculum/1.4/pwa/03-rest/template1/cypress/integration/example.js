describe("Example", function() {
	it('is successful', function() {
		cy.request({
			url: '/example',
		}).then(response => {
			expect(response).property('body').property('success').to.equal(true);
		});
	});

    it('has no root page', function() {
		cy.request({
			url: '/',
			failOnStatusCode: false
		}).then(response => {
			expect(response).property('status').to.equal(404);
			expect(response).property('body').property('error').to.equal("Invalid resource");
		});
	});
});
