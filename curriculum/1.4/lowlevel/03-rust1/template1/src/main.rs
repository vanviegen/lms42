use std::io::Read;
use show_image::{ImageInfo, make_window};

mod bit_reader;


fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage: viewer <filename>");
        std::process::exit(1);
    }

    let data = get_file_as_byte_vec(&args[1]);
    let data_len = data.len();

    let start_time = std::time::Instant::now();
    let (width, height, pixels) = decode_image(data);

    println!("Decompressed {} kb of img42 data into {} kb of pixel data in {}s",
        data_len / 1024,
        pixels.len() / 1024,
        start_time.elapsed().as_millis() as f64 / 1000.0
    );

    // Create a window and display the image
    let window = make_window("image").expect("Couldn't create window");
    window.set_image((pixels, ImageInfo::rgb8(width, height)), "image-001")
        .expect("Couldn't display image");
    
    // Wait for the window to close
    while let Ok(_) = window.wait_key(std::time::Duration::from_secs(3600)) {
    }
}


fn decode_image(data: Vec<u8>) -> (usize, usize, Vec<u8>) {
    let mut br = bit_reader::BitReader::new(data);

    // Just ignore checking the magic string.
    br.get_number(8*5, false);

    let width = br.get_number(16, false) as usize;
    let height = br.get_number(16, false) as usize;

    let mut current_colors = [0u8; 3];
    let mut pixels: Vec<u8> = Vec::new();

    for _ in 0..width*height {
        if br.get_bit() == 0 { // Not identical
            for color in current_colors.iter_mut() {
                if br.get_bit() != 0 { // Use 3-bit delta
                    *color = ((*color as i16) + (br.get_number(3, true) as i16)) as u8;
                } else {
                    *color = br.get_number(8, false) as u8;
                }
            }
        }
        for color in current_colors.iter_mut() {
            pixels.push(*color);
        }
    }

    return (width, height, pixels);
}


fn get_file_as_byte_vec(filename: &String) -> Vec<u8> {
    let mut f = std::fs::File::open(&filename).expect("File not found");
    let metadata = std::fs::metadata(&filename).expect("Unable to read metadata");
    let mut buffer = vec![0; metadata.len() as usize];
    f.read(&mut buffer).expect("Buffer overflow");

    buffer
}