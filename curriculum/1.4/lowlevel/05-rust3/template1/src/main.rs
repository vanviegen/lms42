use rand::Rng;

trait Hashable : Eq {
    fn get_hash(&self) -> usize;
}

impl Hashable for &str {
    fn get_hash(&self) -> usize {
        let mut result : usize = 0;
        for chr in self.bytes() {
            result = result.rotate_left(3) ^ chr as usize;
        }
        result
    }
}

impl Hashable for u32 {
    fn get_hash(&self) -> usize {
        *self as usize
    }
}


struct Hash<T: Hashable> {
    vector: Vec<Option<T>>,
    count: usize,
}

impl<T: Hashable> Hash<T> {
    fn new() -> Hash<T> {
        return Hash {
            vector: Vec::new(),
            count: 0,
        }
    }

    fn add(&mut self, item: T) {
        self.count += 1;
        if self.count*3 > self.vector.len()*2 {
            let mut new_vec : Vec<Option<T>> = vec![None; self.count*3];
            println!("Reindex {} -> {}", self.vector.len(), new_vec.len());
            for item in self.vector.drain(..) {
                if let Some(item) = item {
                    Hash::add_to_vec(&mut new_vec, item);
                }
            }
            self.vector = new_vec;
        }
        Hash::add_to_vec(&mut self.vector, item);
    }

    fn add_to_vec(vector: &mut Vec<Option<T>>, item: T) {
        let start_pos = item.get_hash() % vector.len();
        let mut pos = start_pos;
        loop {
            if let Some(existing) = &vector[pos] { // Check for duplicate
                if item == *existing { // Already exists
                    return
                }
            } else { // Found an empty spot!
                vector[pos] = Some(item);
                return;
            }
            pos = (pos+1) % vector.len();
            assert!(pos != start_pos);
        }
    }

    fn contains(&self, needle: T) -> bool {
        let start_pos = needle.get_hash() % self.vector.len();
        let mut pos = start_pos;
        loop {
            if let Some(item) = &self.vector[pos] {
                if needle==*item {
                    println!("Found at pos={} start_pos={} len={}", pos, start_pos, self.vector.len());
                    return true
                }
            } else { // None
                return false
            }
            pos = (pos+1) % self.vector.len();
            assert!(pos != start_pos);
        }
    }
}

fn main(){
    // You can use this to experiment, or just leave this empty.
}

/*
#[test]
fn u32_is_hashable() {
    assert_eq!(17.get_hash(), 17.get_hash());
    assert_ne!(17.get_hash(), 18.get_hash());
}
*/


#[test]
fn hash_works_with_u32() {
    let mut hash = Hash::new();

    hash.add(1);
    hash.add(1234);
    hash.add(12345678);
    assert!(hash.contains(1));
    assert!(hash.contains(1234));
    assert!(hash.contains(12345678));
    assert!(!hash.contains(2));
    assert!(!hash.contains(1235));
    assert!(!hash.contains(12345679));
}

#[test]
fn hash_scales_with_u32() {
    let mut rng = rand::thread_rng();
    let mut hash = Hash::new();
    let mut reference = std::collections::HashSet::new();

    for _ in 0..500_000 {
        let n = rng.gen_range(0..2_000_000) as u32;
        hash.add(n);
        reference.insert(n);
    }

    for num in 0..2_000_000 {
        assert_eq!(hash.contains(num), reference.contains(&num));
    }
}

#[test]
fn str_ref_is_hashable() {
    assert_eq!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxyz".get_hash());
    assert_ne!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxy".get_hash());
    assert_ne!("abcdefghijklmnopqrstuvwxyz".get_hash(), "abcdefghijklmnopqrstuvwxyZ".get_hash());
}

#[test]
fn hash_works_with_str_ref() {
    let mut hash = Hash::new();

    hash.add("test");
    hash.add("abcd");

    assert!(hash.contains("test"));
    assert!(hash.contains("abcd"));
    assert!(!hash.contains("tset"));
    assert!(!hash.contains("abc"));
    assert!(!hash.contains("abce"));
    assert!(!hash.contains("abcde"));
}

/*
/// This test does not compile. Fix it by moving a single line. 
/// Explain in comments why this change is required.
#[test]
fn hash_works_with_lifetimes() {
    let mut hash : Hash<&str> = Hash::new();

    let mut x = String::from("abc");
    x.push_str("123");

    hash.add(&x);

    x.push_str("456");

    assert!(hash.contains("abc123"));

    assert_eq!(x, "abc123456");
}
*/