from urllib.parse import urlparse, urljoin
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import flask_login
import flask
import re
import datetime
import os

from ..app import db, app
from ..models.user import User, LoginLink
from ..email import send as send_email
from ..utils import generate_password


class LoginForm(flask_wtf.FlaskForm):
    pwd = os.environ.get('VISITOR_PASSWORD')
    email = wtf.StringField('Email', validators=[] if pwd else [wtv.Email(), wtv.DataRequired()])
    submit = wtf.SubmitField('Send login link')

    if pwd:
        password = wtf.StringField("Or visitor's password", validators=[])
        submit_password = wtf.SubmitField('Login by password')

    pass


def is_safe_url(target):
    ref_url = urlparse(flask.request.host_url)
    test_url = urlparse(urljoin(flask.request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


@app.route('/user/login', methods=['GET','POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if 'submit_password' in form and form.submit_password.data:
            if form.password.data == os.environ.get('VISITOR_PASSWORD'):
                user = db.session.query(User).filter_by(email="visitor@role.invalid").first()
                if user==None:
                    # Create the visitor user
                    user = User()
                    user.first_name = 'Visitor'
                    user.last_name = ''
                    user.short_name = 'visitor'
                    user.email = 'visitor@role.invalid'
                    user.level = 30
                    db.session.add(user)
                    db.session.commit()
                flask_login.login_user(user)
                return flask.redirect(flask.request.args.get('next', '/curriculum'))
            else:
                flask.flash("Incorrect password.")

        else:
            tilde_login = False
            email = form.email.data
            if email and email[0]=='~' and os.environ.get('ALLOW_TILDE_LOGIN')=='yes':
                email = email[1:]
                tilde_login = True
            user = db.session.query(User).filter_by(email=email).first()
            
            if user==None and User.query.get(1) == None:
                # Create the admin user with this email address
                user = User()
                user.id = 1
                user.first_name = 'Administrator'
                user.last_name = ''
                user.short_name = 'admin'
                user.level = 90
                user.email = email
                db.session.add(user)

            if user and user.level > 0:
                redirect_url = flask.request.args.get('next')
                if redirect_url and not is_safe_url(redirect_url):
                    redirect_url = None

                if tilde_login:
                    flask_login.login_user(user)
                    return flask.redirect(redirect_url if redirect_url else "/curriculum")
                    
                link = LoginLink()
                link.user = user
                link.secret = generate_password()
                link.redirect_url = redirect_url

                db.session.add(link)
                db.session.commit()

                url = flask.request.url_root + f"user/link/{link.id}/{link.secret}"

                send_email(email, "Your login link", f"""
Hi {user.first_name},

You can use this link to sign into LMS42:

{url}

Best,
Admin.""")

                flask.flash(f"A login link has been sent to: {email}.")
                return flask.render_template('layout.html', title="Link sent")

            flask.flash("This address is not registered.")
    return flask.render_template('generic-form.html', form=form, action="/user/login", title="Log in")


@app.route('/user/link/<id>/<secret>', methods=['GET'])
def login_link(id, secret):
    #if flask.request.method == "HEAD" or flask.request.headers.get("Sec-Fetch-User") == "?1":
    #    # This is a (Outlook safe link) preview - we should ignore it.
    #    flask.flash("Ignoring preview of login link")
    #    return flask.redirect("/") 
    
    # Expire old tokens
    LoginLink.query.filter(LoginLink.time <  datetime.datetime.utcnow()+datetime.timedelta(hours=-1)).delete()
    db.session.commit()

    # Load and verify this token
    link = LoginLink.query.get(id)
    if link != None and link.secret == secret:
        #db.session.delete(link)
        flask_login.login_user(link.user)
        #db.session.commit()
        return flask.redirect(link.redirect_url if link.redirect_url else "/curriculum")
    else:
        flask.flash("The login link has expired (1 hour) or has already been used.")
        return flask.redirect("/user/login")


@app.route('/user/logout', methods=["POST"])
def logout():
    flask_login.logout_user()
    return flask.redirect('/')
