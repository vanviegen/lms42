from ..app import db, app
from ..models.user import User
from ..models.attempt import Attempt
from ..models import curriculum
from .. import working_days
import flask
import datetime
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import sqlalchemy as sa
from flask_login import login_required, current_user

@app.route('/people', methods=['GET','POST'])
@login_required
def list():
    people = User.query.filter(User.is_active==True, sa.or_(User.class_name==None, User.class_name!="test")).order_by(User.class_name, User.first_name).all()

    return flask.render_template('people-list.html', people=people)


def get_progress_graph(user_id):
    days_per_month = working_days.calculate_per_month(datetime.date(2021,2,1))

    month_positions = {month: position for position, month in enumerate(days_per_month)}
    labels = [month for month in days_per_month]

    sql = f"""select month, short_name as name, sum(avg_days) as progress
    from (
        select u.short_name, max(a.avg_days) avg_days, a.node_id, to_char(min(a.submit_time), 'YYYY-MM') as month
        from attempt a
        join "user" u on a.student_id=u.id
        where a.status='passed' and a.student_id = {int(user_id)}
        group by u.id, a.node_id
    ) as passed
    group by short_name, month
    order by short_name, month
    """

    series = [
        {"name": "progress", "data": [0 for _ in days_per_month]},
        {"name": "hours", "data": [0 for _ in days_per_month]}
    ]
    for row in db.engine.execute(sql):
        month = row['month']
        if month in month_positions:
            series[0]["data"][month_positions[month]] = round(100 * row["progress"] / days_per_month[month])

    sql = sa.text("""
    select
        to_char(date, 'YYYY-MM') as month,
        extract(epoch from sum(end_time-start_time))+30*count(*) seconds
    from time_log
    where user_id = :user_id and date < date(now())
    group by month
    """)
    if "2021-06" in days_per_month:
        days_per_month["2021-06"] -= 7 # days before time_log started
    for row in db.engine.execute(sql, user_id=user_id):
        month = row['month']
        if month in month_positions:
            series[1]["data"][month_positions[month]] = round(100 * row["seconds"] / days_per_month[month] / 8 / 60 / 60)

    print("get_progress_graph", series, flush=True)
    return {"labels": labels, "series": series}


class EditForm(flask_wtf.FlaskForm):
    first_name = wtf.StringField('First name', validators=[
        wtv.DataRequired()
    ])

    last_name = wtf.StringField('Last name', validators=[
    ])

    email = wtf.StringField('Email', validators=[
        wtv.Email(), wtv.DataRequired()
    ])

    short_name = wtf.StringField('Short name', validators=[
        wtv.DataRequired(), wtv.Regexp('^[a-z0-9\\-]{2,}$', 0, 'Only numbers, lower case English letters and dashes are allowed.')
    ])

    class_name = wtf.StringField('Class', validators=[], default='ESD1V.')

    level = wtf.IntegerField('Level', validators=[], default=10)

    submit = wtf.SubmitField('Save')


@app.route('/people/<int:userId>', methods=['GET','POST'])
@login_required
def edit(userId):
    person = User.query.get(userId)
    privileged = current_user.is_teacher or current_user.id == userId
    
    edit_form = None
    if current_user.is_admin:
        edit_form = EditForm(obj=person)
        if edit_form.validate_on_submit():
            try:
                edit_form.populate_obj(person)
                db.session.commit()
                return flask.redirect('/people')
            except sa.exc.IntegrityError:
                db.session.rollback()
                edit_form.email.errors.append('Email address already in use.')

    graph = get_progress_graph(userId) if privileged else None

    mentees = []
    nodes_by_id = curriculum.get('nodes_by_id')
    for attempt in Attempt.query.filter_by(status="in_progress", mentor_id=userId):
        node = nodes_by_id[attempt.node_id]
        mentees.append({
            'name': attempt.student.full_name,
            'node': f"{node['name']} ({node['module_id']})",
            'node_id': attempt.node_id,
        })

    return flask.render_template('people-detail.html', person=person, edit_form=edit_form, update_avatar=privileged, graph=graph, mentees=mentees)


@app.route('/people/new', methods=['GET','POST'])
@login_required
def new():
    if not current_user.is_admin:
        return 'Only admins can do that.', 403

    edit_form = EditForm()
    if edit_form.validate_on_submit():
        try:
            person = User()
            edit_form.populate_obj(person)
            db.session.add(person)
            db.session.commit()
            return flask.redirect(f"/people/{person.id}")
        except sa.exc.IntegrityError:
            db.session.rollback()
            edit_form.email.errors.append('Email address already in use.')

    return flask.render_template('people-detail.html', person={}, edit_form=edit_form)


@app.route('/people/<int:userId>/avatar', methods=['POST'])
@login_required
def upload_avatar(userId):
    if current_user.is_teacher or current_user.id == userId:
        user = User.query.get(userId)
        user.avatar = flask.request.form['avatar']
        db.session.commit()
    else:
        flask.flash("Permission denied.")
    return flask.redirect(f"/people/{userId}")
