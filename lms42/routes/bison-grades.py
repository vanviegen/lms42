from ..app import db, app
from ..utils import role_required, format_date
import flask

QUERY = """
SELECT
    node_id, s.first_name, s.last_name, date(a.submit_time), status,
    (
        SELECT g.grade
        FROM grading g 
        WHERE g.attempt_id=a.id 
        ORDER by g.id desc
        LIMIT 1
    ) grade
FROM attempt a 
JOIN "user" s on a.student_id=s.id
WHERE credits>0 and status!='ignored'
ORDER by date, node_id
"""


@app.route('/bison-grades', methods=['GET'])
@role_required('teacher')
def bison_export():
    grades = db.engine.execute(QUERY)
    return flask.render_template('bison-grades.html', grades=grades)
