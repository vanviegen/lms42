from .. import utils, working_days
from ..app import db, app
from ..assignment import Assignment
from ..models import curriculum
from ..models.attempt import Attempt, get_notifications, FORMATIVE_ACTIONS
from ..models.user import User
from ..models.feedback import NodeFeedback
from .. import email
from flask_login import login_required, current_user
from flask_wtf.csrf import generate_csrf
from pathlib import Path
from math import ceil
import wtforms as wtf
import wtforms.validators as wtv
import flask_wtf
import datetime
import flask
import json
import json
import os
import random
import shutil
import urllib
import subprocess


DEFAULT_GIT_IGNORE=""".venv
node_modules
build
"""


class SubmitForm(flask_wtf.FlaskForm):
    resource_quality = wtf.RadioField('What did you think about the provided learning resources?', choices = [
        (1,'Terrible'),
        (2,'Not so good'),
        (3,'Just okay'),
        (4,'Pretty good'),
        (5,'Excellent'),
    ])

    assignment_clarity = wtf.RadioField("Was it clear what the assignment wanted you to do?", choices = [
        (1,'Not at all'),
        (2,'No'),
        (3,'A bit vague'),
        (4,'Mostly clear'),
        (5,'Perfectly clear'),
    ])

    difficulty = wtf.RadioField("How difficult did you find the assignment?", choices = [
        (1,'Way too easy'),
        (2,'Too easy'),
        (3,'Just right'),
        (4,'Too hard'),
        (5,'Way too hard'),
    ])

    hours = wtf.IntegerField("How many (effective) hours did you spent on the assignment and learning resources?")

    fun = wtf.RadioField("Did you enjoy working on the assignment?", choices = [
        (1,'Hated every moment'),
        (2,'No'),
        (3,'It was okay'),
        (4,'For the most part'),
        (5,'It was fun!'),
    ])

    comments = wtf.TextAreaField("Any further comments to help us improve?")

    discard = wtf.SubmitField('Discard attempt', render_kw={'class': 'button is-danger'})
    submit = wtf.SubmitField('Submit attempt')



@app.route('/curriculum', methods=['GET'])
def curriculum_get():
    student = get_student()
    periods = curriculum.get_periods_with_status(student)

    # Find the appropriate period tab to start with
    initial_block = "1.1"
    for block_name, block in periods.items():
        for topic in block:
            if topic.get("status") == "progress":
                initial_block = block_name
                break

    return flask.render_template('curriculum.html',
        student=student,
        periods=periods,
        nodes_by_id=curriculum.get('nodes_by_id'),
        modules_by_id=curriculum.get('modules_by_id'),
        initial_block=initial_block,
        errors=curriculum.get('errors'),
        warnings=curriculum.get('warnings'),
        stats=curriculum.get("stats") if current_user.is_authenticated and current_user.is_inspector else None,
    )



@app.route('/attempts/<attempt_id>/approve', methods=['POST'])
@utils.role_required('teacher')
def approve_attempt(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    if attempt.status == "awaiting_approval":
        attempt.status = "in_progress"
        db.session.commit()
    else:
        flask.flash("Approval was not needed.")
    return flask.redirect("/inbox")


@app.route('/attempts/<attempt_id>/consent', methods=['POST'])
@utils.role_required('teacher')
def consent_grading(attempt_id):
    attempt = Attempt.query.get(attempt_id)
    grading = attempt.latest_grading

    if attempt.status != "needs_consent":
        flask.flash("No consent is required.")

    elif grading.grader_id == current_user.id:
        flask.flash("You cannot give consent to a grade you determined yourself.")
    
    else:
        grading.needs_consent = False
        grading.consent_user_id = current_user.id
        grading.consent_time = datetime.datetime.now()

        ao = Assignment.load_from_directory(attempt.directory)
        _, passed = ao.calculate_grade(grading.objective_scores)
        attempt.status = "passed" if passed else "failed"

        db.session.commit()

    return flask.redirect("/inbox")


def find_mentor_id(student, node_id, variant_id):
    # Determine the day of the week that the work will (mostly) take place, 
    # so that we can filter out students that are absent on that day.
    now = datetime.datetime.today()
    weekday = now.weekday()
    if now.hour >= 15:
        weekday += 1
    if weekday > 4: # > Friday
        weekday = 0 # Monday
    
    result = db.session.execute("""
        select id, badness
        from (
            select u.id, (
                coalesce(max(case when
                    a.status='passed' then 0
                    when a.status='needs_grading' then 20
                    else 10
                end), 0) +
                (
                    select coalesce(sum(case
                        when t.status='in_progress' then 100000
                        when t.student_id = :student_id then 3
                        else 1
                    end), 0)
                    from attempt t
                    where t.mentor_id=u.id
                )
            ) as badness
            from attempt a 
            join "user" u on u.id = a.student_id
            where a.node_id = :node_id
                and a.variant_id = :variant_id
                and (
                    a.status='passed'
                    or a.status='needs_grading'
                    or (a.status='in_progress' and a.mentor_id is null)
                )
                and a.student_id != :student_id
                and u.level = 10
                and u.class_name = :class_name
                and not (:weekday = any (u.absent_days))
                and not exists(select 1 from attempt t where t.id=u.current_attempt_id and t.credits>0)
            group by u.id
        ) as user_badness
        where badness < 300000
        order by badness asc
        limit 1
    """, {
        "student_id": student.id,
        "node_id": node_id,
        "variant_id": variant_id,
        "weekday": weekday,
        "class_name": student.class_name,
    })

    for row in result:
        return row["id"]


@app.route('/curriculum/<node_id>', methods=['POST'])
@login_required
def node_action(node_id):
    node = curriculum.get_node_with_status(node_id, current_user)
    if "start" in flask.request.form:
        errors, _ = check_node_startable(node)
        if current_user.current_attempt:
            errors.append("You are already working on an assignment.")
        if errors:
            flask.flash(" ".join(errors))
            return flask.redirect(flask.request.url)
        
        last_number = db.session.query(Attempt.number) \
            .filter_by(student_id=current_user.id, node_id=node["id"]) \
            .order_by(Attempt.number.desc()).first()
        if last_number: # For some reason, a tuple is returned.
            last_number = last_number[0]
        else:
            last_number = 0

        deadline = None
        if not node.get('allow_longer'):
            deadline = datetime.datetime.now()
            if 'ects' in node:
                # Some time on the day of the deadline, in local time:
                deadline += datetime.timedelta(days=(node['days']-1))
                # 18:00 on the day of the deadline, in local time:
                deadline = deadline.replace(hour = 18, minute = 0, second = 0, microsecond = 0)
            else:
                half_days = node['days'] * 3 # set deadline to 3 half-days for each recommended day of work
                half_days -= 0 if deadline.hour >= 14 else 1 if deadline.hour >= 10 else 2 # subtract today's half days
                delta_days = ceil(half_days / 2)

                if delta_days:
                    day = working_days.offset(deadline.date(), delta_days)
                    deadline = datetime.datetime(day.year, day.month, day.day)

                if half_days % 2 == 1:
                    deadline = deadline.replace(hour = 12, minute = 30, second = 0, microsecond = 0)
                else:
                    deadline = deadline.replace(hour = 18, minute = 0, second = 0, microsecond = 0)

            # Convert to UTC
            deadline = utils.local_to_utc(deadline)

        # Select a semi-random variant giving preference to variants that the student has
        # attempted the least often.
        variant_occurrences = {}
        variant_id = 1
        while node.get(f'assignment{variant_id}'):
            # By default, take the first assignment first, unless it's an exam, than take a random assignment
            variant_occurrences[variant_id] = random.uniform(0, 1) if "ects" in node else variant_id/10
            variant_id += 1

        attempts = Attempt.query.filter_by(student_id=current_user.id, node_id=node["id"])
        for attempt in attempts:
            if attempt.variant_id in variant_occurrences and attempt.status != "repair":
                variant_occurrences[attempt.variant_id] += 1

        if not variant_occurrences:
            flask.flash("Sorry, there is no assignment available. Talk to a teacher!")
            return flask.redirect(flask.request.url)

        variant_id = sorted(variant_occurrences.items(), key=lambda x: x[1])[0][0]
        assignment = node.get(f'assignment{variant_id}')

        mentor_id = None if "ects" in node else find_mentor_id(current_user, node["id"], variant_id)

        attempt = Attempt(
            student_id = current_user.id,
            number = last_number + 1,
            node_id = node["id"],
            variant_id = variant_id,
            deadline_time = deadline,
            status = "awaiting_approval" if "ects" in node else "in_progress",
            credits = node["ects"] if "ects" in node else 0,
            avg_days = node["avg_attempts"] * node["days"],
            mentor_id = mentor_id,
        )

        for _ in range(5):
            try:
                os.mkdir(attempt.directory)
                break
            except FileExistsError:
                attempt.number += 1
                pass
        else:
            print(f"Failed to create {attempt.directory}")
            flask.flash(f"Error! Could not create attempt directory: {attempt.directory}")
            return flask.redirect(flask.request.url)

        db.session.add(attempt)
        db.session.commit() # causes attempt.id to be available
        current_user.current_attempt_id = attempt.id
        db.session.commit()

        # Write the attempt as json
        attempt.write_json()

        # Write the assignment as json
        with open(attempt.directory+"/assignment.json", "w") as file:
            file.write(json.dumps(assignment, indent=4))

        # Strip out useles data from the node, and write it to json
        node_copy = node.copy()
        variant_id2 = 1
        while node_copy.get(f'assignment{variant_id2}'):
            node_copy.pop(f'assignment{variant_id2}')
            variant_id2 += 1
        for name in ['directory', 'attempts', 'status']:
            node_copy.pop(name)

        with open(attempt.directory+"/node.json", "w") as file:
            file.write(json.dumps(node_copy, indent=4))

        # Copy the solution if it exists
        sol_dir = f"{node['directory']}/solution{variant_id}"
        if Path(sol_dir).is_dir():
            shutil.copytree(sol_dir, attempt.directory+"/solution")

        # Copy the template if it exists
        tmpl_src = f"{node['directory']}/template{variant_id}"
        tmpl_dst = attempt.directory+"/template"
        if Path(tmpl_src).is_dir():
            shutil.copytree(tmpl_src, tmpl_dst)
        else:
            os.mkdir(tmpl_dst)

        # Create a default .gitignore file if it doesn't exist
        gitignore = f"{tmpl_dst}/.gitignore"
        if not Path(gitignore).is_file():
            with open(gitignore, "w") as file:
                file.write(DEFAULT_GIT_IGNORE)

        # Do a git init in the template directory
        subprocess.run(["git", "init"], cwd=tmpl_dst)
        subprocess.run(["git", "add", "."], cwd=tmpl_dst)
        subprocess.run(["git", "commit", "--author", "sd42 <info@sd42.nl>", "-m", "Teacher-provided template"],
            cwd=tmpl_dst,
            env={'GIT_COMMITTER_NAME': 'sd42', 'GIT_COMMITTER_EMAIL': 'info@sd42.nl'}
        )

        if mentor_id != None:
            mentor = User.query.get(mentor_id)
            email.send(mentor.email, f"New mentee: {current_user.first_name}", f"""
            Hi {mentor.first_name},

            {current_user.first_name} just started working on {node["name"]} ({node["module_id"]}). Would you please be her/his mentor for this?

            Thanks!
            SD42
            """)

        return flask.redirect('#new')

    elif "submit" in flask.request.form:
        attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="in_progress").first()
        if attempt:
            # Update the attempt state
            attempt.status = "needs_grading"
            attempt.submit_time = datetime.datetime.utcnow()
            if current_user.current_attempt_id == attempt.id:
                current_user.current_attempt_id = None

            # Save the feedback
            submit_form = SubmitForm()
            if submit_form.validate_on_submit():
                feedback = NodeFeedback()
                submit_form.populate_obj(feedback)
                feedback.node_id = node_id
                feedback.student_id = current_user.id
                db.session.merge(feedback)

            # Flush
            db.session.commit()
            attempt.write_json()

            flask.flash("Assignment submitted!")
        else:
            flask.flash("Assignment already submitted?  ")
        return flask.redirect('/curriculum')

    elif "discard" in flask.request.form:
        node = curriculum.get('nodes_by_id')[node_id]
        if "ects" in node:
            flask.flash("Exam attempts cannot be discarded.")
        else:
            attempt = Attempt.query.filter_by(student_id=current_user.id, node_id=node_id, status="in_progress").first()
            if attempt:
                if current_user.current_attempt_id == attempt.id:
                    current_user.current_attempt_id = None
                    # Why is this extra commit necessary SQLAlchemy? Why!?
                    db.session.commit()

                db.session.delete(attempt)
                db.session.commit()

                shutil.rmtree(attempt.directory)

                flask.flash("Attempt discarded.")
            else:
                flask.flash("No such attempt.")
        return flask.redirect('/curriculum')
    
    else:
        raise Exception(f"Invalid action {flask.request.form}")


@app.route('/curriculum/<node_id>/<int:variant_id>/template.zip', methods=['GET'])
def node_get_template(node_id, variant_id):
    node = curriculum.get('nodes_by_id')[node_id]
    if node.get('public',False)==True or (current_user.is_authenticated and (current_user.is_inspector or node.get('public')=='users')):
        zip = subprocess.Popen(['zip', '-r', '-', f'template{variant_id}'], stdout=subprocess.PIPE, cwd=node["directory"])
        return flask.Response(zip.stdout, content_type='application/zip')
    else:
        return 'Lesson is not public.', 403


def get_all_variants_info(node):
    assignments = {}
    variant = 1
    while node.get(f'assignment{variant}'):
        ao = Assignment(node[f"assignment{variant}"], node)

        notifications = []
        if os.path.isdir(f"{node['directory']}/template{variant}"):
            notifications.append(f"""<a class="button is-primary" href="/curriculum/{node['id']}/{variant}/template.zip">Download template</a>""")

        assignments[f"Variant {variant}"] = {
            "notifications": notifications,
            "html": ao.render(current_user.is_authenticated and current_user.is_inspector),
        }
        variant += 1
    return assignments


@app.route('/curriculum/<node_id>', methods=['GET'])
def node_get(node_id):
    student = get_student()
    status = None
    node = curriculum.get_node_with_status(node_id, student)

    assignments = {}
    if current_user.is_authenticated and student:
        # Show all attempts for a specific student in tabs
        for attempt in reversed(node["attempts"]):
            regrade = int(flask.request.args.get("regrade","-1")) == attempt.id
            assignments[f"Attempt {attempt.number}"] = get_attempt_info(node, attempt, student, regrade)

        if not assignments and node.get("public", False)=="users":
            assignments = get_all_variants_info(node)

        if node["status"] != "in_progress":
            errors, warnings = check_node_startable(node)
            status = {'action': 'start', 'errors': errors, 'warnings': warnings}

    elif not student and ((current_user.is_authenticated and current_user.is_inspector) or node.get('public',False)==True):
        # View assignment, not attached to a specific student
        assignments = get_all_variants_info(node)
    else:
        status = {'action': 'login'}


    return flask.render_template('node.html',
        topic=curriculum.get('modules_by_id')[node["module_id"]],
        student=student,
        node=node,
        assignments=assignments,
        status=status
    )


def get_attempt_info(node, attempt, student, force_new_grading=False):
    """View assignment attempt for a specific student."""

    html = ''
    submit_form = None
    submit_warnings = []

    if current_user == student and attempt.status == "awaiting_approval":
        notifications = [
            "Ask a teacher to approve starting the exam, and then reload this page.",
            "Please review the <a href='/coc#exams' target='_blank'>exam CoC</a> before you start.",
        ]
    else:
        needs_grading = (attempt.status == "needs_grading") or force_new_grading
        show_rubrics = (True if needs_grading else "disabled") if current_user.is_teacher else False
        
        # Load the grades, if any
        latest_grading = attempt.latest_grading
        if latest_grading==None and needs_grading and current_user.is_teacher:
            # Help the teacher by prefilling the grading from a previous attempt that needed to be repaired.
            prev_attempt = Attempt.query.filter(
                Attempt.student_id == attempt.student_id,
                Attempt.node_id == attempt.node_id,
                Attempt.variant_id == attempt.variant_id,
                Attempt.status != "needs_grading",
            ).order_by(Attempt.submit_time.desc()).first()
            if prev_attempt and prev_attempt.status == "repair":
                latest_grading = prev_attempt.latest_grading

        # Render the actual assignment to HTML
        ao = Assignment.load_from_directory(attempt.directory)
        if not current_user.is_teacher and "ects" in node and attempt.status != "in_progress":
            html = "<div>Exams can only be viewed while taking them. Questions about the grading? Ask a teacher!</div>" + \
                ao.render_grading(ao.assignment, ao.goals, show_rubrics, latest_grading)
        else:
            html = ao.render(show_rubrics, latest_grading)

        notifications = get_notifications(attempt, latest_grading)

        if current_user.is_teacher:
            if needs_grading:
                html = f'''
                    <form method="post" action="/inbox/grade/{attempt.id}" >
                        {html}
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <h1>Finalize grading</h1>
                        {"" if "ects" in node else render_formative_action(attempt.status)}
                        <textarea placeholder="Motivation..." class="textarea" name="motivation">{flask.escape(latest_grading.grade_motivation) if latest_grading and latest_grading.grade_motivation else ''}</textarea>
                        <div class="buttons is-right mt-2">
                            <input class="button is-primary" type="submit" value="Publish">
                        </div>
                    </form>
                '''
            elif attempt.status == "awaiting_approval":
                notifications.append(f'''
                    Start of this exam needs to be approved.
                    <form method="post" action="/attempts/{attempt.id}/approve" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <input class="button" type="submit" name="approve" value="Approve">
                    </form>
                ''')
            elif attempt.status in ["needs_consent", "passed", "repair", "failed"]:
                if attempt.status == "needs_consent":
                    consent = '<input class="button is-primary" type="submit" name="consent" value="Consent grade">'
                else:
                    consent = ''
                html += f'''
                    <form method="post" action="/attempts/{attempt.id}/consent" class="buttons is-right mt-2 is-primary">
                        <input type="hidden" name="csrf_token" value="{generate_csrf()}">
                        <a class="button" href="/curriculum/{node['id']}?student={attempt.student_id}&regrade={attempt.id}">Regrade</a>
                        {consent}
                    </form>
                '''

        if attempt.status == "in_progress" and current_user == student:
            # It's the actual student, show start/submit actions when appropriate
            current_attempt = student.current_attempt

            if current_attempt and current_attempt.node_id == attempt.node_id:
                if not current_attempt.upload_time:
                    submit_warnings.append("You haven't uploaded any work yet.")
                elif current_attempt.upload_time + datetime.timedelta(minutes=15) < datetime.datetime.utcnow():
                    submit_warnings.append("Your last upload was more than 15 minutes ago.")

                old_values = NodeFeedback.query.filter_by(node_id=attempt.node_id, student_id=student.id).first()
                submit_form = SubmitForm(obj=old_values)
                    # - change the form instances to include anyway and set the primary class conditionally
                if submit_warnings:
                    submit_form.submit.label.text += " anyway"
                    submit_form.submit.render_kw={'class': 'button'}

    return {
        "html": html,
        "notifications": notifications,
        "submit_form": submit_form,
        'submit_warnings': submit_warnings,
    }


def render_formative_action(status):
    options = [f'<option value="{k}"{" selected" if k==status else ""}>{v}</option>' for k,v in FORMATIVE_ACTIONS.items()]
    return f'''
        <div class="select mb-2 mt-2">
            <select name="formative_action" required>
                <option value="">What should happen?</option>
                {"".join(options)}
            </select>
        </div>
    '''



def check_node_startable(node):
    status = node["status"]
    warnings = []
    errors = []
    if status == "startable":
        pass
    elif status == "future":
        warnings.append("At least one previous node hasn't been completed yet.")
    elif status == "needs_grading" or status == "needs_consent":
        warnings.append("You have already submitted this assignment, but it hasn't been graded yet.")
    elif status == "passed":
        warnings.append("There's no need to start this assignment again.")
    else:
        errors.append(f"Unknown status: {status}.")

    if datetime.datetime.now().hour >= 16:
        warnings.append("It's too late in the day to start a new assignment. Why not use this time to hack on a fun project?")

    if current_user and current_user.current_attempt:
        errors.append("You are already working on another assignment.")

    if node.get("start_when_ready"):
        warnings.append("As you'll want to interleave other modules with your work on this assignment, you should wait pressing the 'Start attempt' button here until you're ready to submit your work.")
    return errors, warnings


LMS_DIR=os.getcwd()


@app.route('/curriculum/<node_id>/static/<name>', methods=['GET'])
# TODO: perhaps require login, or do even more stringent checks? This would break
# the HTML inliner used to create a self-contained attempt.html file though.
def node_file(node_id, name):
    node = curriculum.get('nodes_by_id')[node_id]
    dir=f"{LMS_DIR}/{node['directory']}/static"
    return flask.send_from_directory(dir, name)



def get_student():
    if current_user:
        student_id = flask.request.args.get('student')
        if student_id and current_user.is_teacher:
            return User.query.get(student_id)
        if current_user.is_authenticated and not current_user.is_inspector:
            return current_user
