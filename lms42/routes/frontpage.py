from ..app import db, app
import flask
from flask_login import login_required, current_user


@app.route('/', methods=['GET'])
def frontpage():
    return flask.render_template('frontpage.html', frontpage=True)


@app.route('/coc', methods=['GET'])
@login_required
def coc():
    return flask.render_template('coc.html', frontpage=True)
