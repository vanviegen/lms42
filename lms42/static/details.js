'use strict';

(function() {
    let expandList = document.getElementsByClassName('details-expand');
    let blockList = document.getElementsByClassName('details-block');
    const GROUP_LABEL = 'details-expand-group'
    
    for(let num=0; num<blockList.length; num++) {
        let show = true;
        function toggle() {
            show = !show;
            if (show && expandList[num].classList.contains(GROUP_LABEL)) {
                // Collapse others
                for(let num=0; num<blockList.length; num++) {
                    blockList[num].style.display = 'none';
                    expandList[num].classList['remove']('shown')
                }
            }
            blockList[num].style.display = show ? '' : 'none';
            expandList[num].classList[show ? 'add' : 'remove']('shown')
        }
        toggle();
        expandList[num].addEventListener('click', toggle);
    }

})();