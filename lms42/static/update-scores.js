(function() {
    if (self.initUpdateScores) return;

    let attempts = [];

    self.initUpdateScores = function(scriptE, all_weights, floor, passing_grade) {
        for(let formE = scriptE; true; formE = formE.parentNode) {
            if (formE.classList.contains("assignment")) {
                attempts.push({formE, all_weights, floor, passing_grade});
                break;
            }
        }
        updateScores();
    };

    self.updateScores = function() {
        for(let {formE, all_weights, floor, passing_grade} of attempts) {
            let scoreRowE = formE.getElementsByClassName(`grading-scores`)[0];
            let gradeRowE = formE.getElementsByClassName(`grading-grade`)[0];
            let selectE = formE.querySelector('[name="formative_action"]') || {};
            let total = floor;
            let entry_fail = false;
            let grade_count = 0;
            let table_pos = 0;
            let incomplete = false;
            for(let rubric_num=0; rubric_num<all_weights.length; rubric_num++) {
                weight = all_weights[rubric_num];
                let radioE = formE.querySelector(`input[name="score_${rubric_num}"]:checked`);
                if (!radioE) {
                    incomplete = true;
                }
                if (weight!=null) {
                    let score = '', grade = '';
                    if (radioE) {
                        score = Math.round(radioE.value*25)+'%';
                        grade = radioE.value/4.0*weight;
                        total += grade;
                        grade = grade.toFixed(1)
                    } else {
                        entry_fail = true;
                    }
                    table_pos++;
                    scoreRowE.children[table_pos].innerText = score;
                    gradeRowE.children[table_pos].innerText = grade;
                    grade_count++;
                } else {
                    if (radioE && radioE.value!="yes") entry_fail = true;
                }
            }
            if (entry_fail || Math.round(total+0.0001) < passing_grade) {
                if (!gradeRowE.classList.contains('failed')) {
                    gradeRowE.classList.add('failed');
                    // Let the teacher decide what to do...
                    selectE.value = '';
                }
            } else {
                gradeRowE.classList.remove('failed');
                if (!incomplete & !selectE.value) {
                    selectE.value = "passed";
                }
            }
            if (incomplete) {
                gradeRowE.classList.add('incomplete');
            } else {
                gradeRowE.classList.remove('incomplete');
            }
            gradeRowE.children[grade_count+2].innerText = total.toFixed(1);
        }
    };
})();
