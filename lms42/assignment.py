import re
import flask
import json
from random import randrange
from .utils import markdown_to_html, url_encode, convert_keys_to_int

# TODO: implement this as a collection of jinja2 macros

class Assignment:

    def __init__(self, assignment, node):
        """Args:
            assignment (dict): Version-specific assignment data structure.
            goals (dict): Learning goals for the node.
        """
        assert(node["format_version"][0]==1)
        self.assignment = assignment
        self.node = node

    @staticmethod
    def load_from_directory(directory):
        with open(f"{directory}/assignment.json", "r") as file:
            assignment = convert_keys_to_int(json.loads(file.read()))
        with open(f"{directory}/node.json", "r") as file:
            node = convert_keys_to_int(json.loads(file.read()))
        return Assignment(assignment, node)

    @property
    def passing_grade(self):
        return 6 if "ects" in self.node else 8

    @property
    def goals(self):
        return self.node["goals"]

    def render(self, show_rubrics, grading=None):
        """Args:
            show_rubrics: True => show rubrics, 'disabled' => shown but disabled, False => 'not shown'
            grading: An optional Grading object containing `grade`, `motivation`, `objective_scores`
                and `objective_motivations`.
        """
        return \
            self.render_requirements(self.assignment["document"], self.assignment["rubrics"], show_rubrics, grading) + \
            self.render_grading(self.assignment, self.goals, show_rubrics, grading)


    def render_requirements(self, objectives, rubrics, show_rubrics, grading=None, depth=1):
        disabled = "disabled " if show_rubrics=="disabled" or show_rubrics==False else ""
        onchange = "onchange='updateScores()' " if show_rubrics==True else ""
        out = ''
        if objectives != None:
            for objective in objectives:
                if isinstance(objective, str):
                    out += markdown_to_html(objective)
                elif "rubric" in objective:
                    pos = objective["rubric"]
                    rubric = rubrics[pos]
                    score = grading and pos < len(grading.objective_scores) and grading.objective_scores[pos]
                    motivation = grading.objective_motivations[pos] if grading and pos < len(grading.objective_motivations) else ''
                    if "range" in rubric:
                        tag = f'<span class="tag is-primary">{self.format_grade(rubric["range"][1]-rubric["range"][0])} points</span>'
                    else:
                        tag = f'<span class="tag is-danger">MUST</span>'
                    out += \
                        f'<article class="objective">' +\
                        f'<div class="body assignment">' +\
                        f'<h2>Objective #{pos+1}{": "+rubric["title"] if "title" in rubric else ""}{tag}</h2>' +\
                        (markdown_to_html(rubric["text"]) if "text" in rubric else "") +\
                        f'</div>'
                    if show_rubrics or grading:
                        out += f'<div class="rubric">'
                        if show_rubrics:
                            if "range" in rubric: # A five point rubric
                                for i in range(0,5): 
                                    out += f'<label>'
                                    out += f'<input required type="radio" {onchange}{disabled}{"checked " if score==i else ""}name="score_{pos}" value="{i}"> '
                                    out += f'<strong>{i}.</strong> {markdown_to_html(rubric.get(i,""))}'
                                    out += f'</label>'
                            else: # An entry condition
                                must = rubric.get("must")
                                if isinstance(must, str):
                                    out += f"<p>{must}</p>"
                                out += f'<label>'
                                out += f'<input required type="radio" {onchange}{disabled}{"checked " if score==0 else ""}name="score_{pos}" value="no"> '
                                out += f'<em>No.</em>'
                                out += f'</label>'
                                out += f'<label>'
                                out += f'<input required type="radio" {onchange}{disabled}{"checked " if score==1 else ""}name="score_{pos}" value="yes"> '
                                out += f'<em>Yes.</em>'
                                out += f'</label>'
                        else: # grading and not show_rubrics
                            color = "success"
                            if "range" in rubric:
                                content = f'{score*25}% 🠖 {self.format_grade((rubric["range"][1]-rubric["range"][0])*score/4)} points'
                                if score < 3:
                                    color = "danger"
                            else:
                                content = "✓" if score else "❌"
                                if not score:
                                    color = "danger"
                            out += f'<div class="has-text-{color} has-text-centered is-primary mb-2">{content}</div>'
                            
                        out += f'<textarea class="textarea" placeholder="Motivation..." {disabled}name="motivation_{pos}">{flask.escape(motivation)}</textarea>'
                        out += f'</div>'
                    out += f'</article>'
                elif "link" in objective:
                    out += self.render_resource(objective)
                else: # it's a section
                    title = f'<h{depth}>{objective["title"]}</h{depth}>' if 'title' in objective else ''
                    out += f'{title}{self.render_requirements(objective["children"], rubrics, show_rubrics, grading, depth+1)}'
        return out


    def render_grading(self, assignment, goals, show_rubrics, grading):
        # Filter out MUST rubrics
        all_rubrics = assignment['rubrics']
        rubrics = [rubric for rubric in all_rubrics if "range" in rubric]

        result = f'<h1>Rubrics mapping and grading</h1>'
        result += f'<table class="map-table"><thead><tr><th></th>'
        for idx, rubric in enumerate(all_rubrics):
            if "range" in rubric:
                result += f'<th>#{idx+1}</th>'
        result += f'<th>-</th><th>Σ</th></tr>\n</thead>'

        result += f'<tbody>'
        for goal_id, goal in goals.items():
            result += f'<tr><td>{goal["title"]}</td>'
            for rubric in rubrics:
                goal_range = rubric["goal_ranges"].get(goal_id)
                if goal_range:
                    result += f'<td>{self.format_grade(goal_range[1] - goal_range[0])}</td>'
                else:
                    result += '<td></td>'
            if goal_id in assignment["goal_ranges"]:
                result += f'<td>{self.format_grade(assignment["goal_ranges"][goal_id][0])}</td><th>{self.format_grade(assignment["goal_ranges"][goal_id][1])}</th></tr>\n'
            else:
                result += f'<td></td><th class="error">{self.format_grade(0)}</th></tr>\n'

        result += f'<tr><td><i>Base grade.</i></td>'
        for rubric in rubrics:
            result += f'<td></td>'
        result += f'<td>{self.format_grade(1)}</td><th>{self.format_grade(1)}</th></tr>\n'

        result += f'<tr class="highlight"><th>Σ</th>'
        grade = 1
        for rubric in rubrics:
            grade += rubric["range"][1]
            result += f'<th>{self.format_grade(rubric["range"][1] - rubric["range"][0])}</th>'
        result += f'<th>{self.format_grade(assignment["floor"])}</th><th>{self.format_grade(grade)}</th></tr>\n'

        
        if grading:
            scores = [grading.objective_scores[idx] if idx<len(grading.objective_scores) else 0 for idx in range(len(all_rubrics))]
        else:
            scores = [0]*len(all_rubrics) if show_rubrics==True else None

        if scores:
            result += f'<tr class="grading-scores"><th>Score</th>'
            for idx, rubric in enumerate(all_rubrics):
                if "range" in rubric:
                    result += f'<th>{round(scores[idx]*25)}%</th>'
            result += '<th>100%</th><th></th></tr>\n'

            all_weights = []
            result += f'<tr class="grading-grade highlight{" failed" if not grading or not grading.passed else ""}"><th>Grade</th>'
            grade = assignment["floor"]
            for idx, rubric in enumerate(all_rubrics):
                if "range" in rubric:
                    weight = rubric["range"][1] - rubric["range"][0]
                    score = scores[idx] / 4 * weight
                    grade += score
                    result += f'<th>{self.format_grade(score)}</th>'
                    all_weights.append(weight)
                else:
                    all_weights.append(None)
            result += f'<th>{self.format_grade(assignment["floor"])}</th><th>{self.format_grade(grade)}</th></tr>\n'

        result += '</tbody></table>'

        if scores and show_rubrics==True:
            result += f'<script src="/static/update-scores.js"></script><script>initUpdateScores(document.currentScript, {json.dumps(all_weights)}, {assignment["floor"]}, {self.passing_grade})</script>'

        return result


    def format_grade(self, grade):
        return '{0:.1f}'.format(grade) if grade<-0.01 or grade>0.01 else ""


    def get_weights(self):
        return [(rubric["range"][1] - rubric["range"][0]) if "range" in rubric else None for rubric in self.assignment['rubrics']]

    def get_floor(self):
        return self.assignment["floor"]

    def form_to_scores(self, form: dict):
        scores = []
        for idx, rubric in enumerate(self.assignment['rubrics']):
            value = form[f"score_{idx}"]
            if "range" in rubric:
                scores.append(int(value))
            else:
                scores.append(1 if value=="yes" else 0)
        return scores

    def form_to_motivations(self, form: dict):
        motivations = []
        for idx in range(len(self.assignment['rubrics'])):
            motivations.append(form[f"motivation_{idx}"])
        return motivations

    def render_resource(self, data):
        html = f'<a class="link_preview" href="{data["link"]}">'
        link = f"/link_preview/{url_encode(data['link'])}"
        html += f'<div class="thumbnail" style="background-image: url({link});"></div><div class="info">'

        m = re.match(r'.*?//(www.)?(.*?)/', data['link'])
        domain = m[2] if m else data['link']
        html += f'<h1>{data.get("title", domain)}</h1>'
        if data.get('info'):
            html += markdown_to_html(data['info'])
        html += f'</div></a>'
        return html

    def calculate_grade(self, scores):
        grade = self.get_floor()
        entry_conditions = True
        for score, weight in zip(scores, self.get_weights()):
            if weight==None:
                if not score:
                    entry_conditions = False
            else:
                grade += weight * score / 4

        # In case entry conditions have not passed, the grade must be at least 1 lower
        # than the passing grade.
        grade = round(grade + 0.0001) # Make sure .5 is rounded up
        grade = max(1, min(10 if entry_conditions else self.passing_grade - 1, grade))
        passed = bool(grade >= self.passing_grade)
        return grade, passed
