import pytz
import datetime
import flask
from flask_login import current_user
import urllib
import hashlib
import os
import functools
import random
import string
import locale
import mistune


local_timezone = pytz.timezone("Europe/Amsterdam")
locale.setlocale(locale.LC_TIME, "en_US.UTF-8")


def utc_to_local(time):
    return time.replace(tzinfo=datetime.timezone.utc).astimezone(local_timezone)

def utc_to_display(time):
    return utc_to_local(time).strftime('%Y-%m-%d at %H:%M')

def local_to_utc(time):
    return local_timezone.localize(time).astimezone(tz=datetime.timezone.utc)

def model_to_dict(model, local_times=True):
    d = {}
    for c in model.__table__.columns:
        v = getattr(model, c.name)
        d[c.name] = v if isinstance(v,int) or isinstance(v,bool) or v==None else (utc_to_local(v) if local_times else v).isoformat(timespec="seconds") if isinstance(v,datetime.datetime) else str(v)
    return d

def role_required(role):
    def decorator(func):
        @functools.wraps(func)
        def decorated_view(*args, **kwargs):
            if not current_user.is_authenticated or not getattr(current_user, 'is_'+role):
                return flask.current_app.login_manager.unauthorized()
            return func(*args, **kwargs)
        return decorated_view
    return decorator

markdown_to_html2 = mistune.create_markdown(escape=False, plugins=['table'])

def markdown_to_html(text):
    return markdown_to_html2((text or '').strip())

def url_encode(s):
    if type(s) == 'Markup':
        s = s.unescape()
    s = s.encode('utf8')
    return urllib.parse.quote(s, safe='')

def convert_keys_to_int(inp):
    """Numeric keys will become strings when converted to json and back.
    Given a dictionary, this function returns a new dictionary that replaces
    all keys that look like an int with actual ints, recursively."""
    if type(inp) == dict:
        new_dict = {}
        for k, v in inp.items():
            try:
                new_key = int(k)
            except ValueError:
                new_key = k
            new_dict[new_key] = convert_keys_to_int(v)
        return new_dict
    elif type(inp) == list:
        new_list = []
        for v in inp:
            new_list.append(convert_keys_to_int(v))
        return new_list
    else:
        return inp

def generate_password(dummy=None):
    return ''.join(random.choice(string.ascii_letters) for _ in range(16))

def format_date(date):
    if isinstance(date,str):
        try:
            date = datetime.date.fromisoformat(date)
        except ValueError:
            return date
    return date.strftime("%a %-d %b %Y")
