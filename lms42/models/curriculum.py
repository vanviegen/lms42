import os
import pickle
from .attempt import Attempt
from copy import deepcopy

cache_stat = None
cache_data = None

def get(key = None):
    global cache_stat
    global cache_data

    stat = os.stat('curriculum.pickle')
    if cache_stat == None or stat.st_size != cache_stat.st_size or stat.st_mtime != cache_stat.st_mtime:
        cache_stat = stat
        with open('curriculum.pickle', 'rb') as file:
            cache_data = pickle.load(file)

    return cache_data.get(key) if key else cache_data


def get_periods_with_status(student):
    if student == None:
        return get('periods')
    
    attempts_by_node = {}
    in_progress = None
    for attempt in Attempt.query.filter_by(student_id = student.id).order_by(Attempt.node_id, Attempt.number.asc()):
        if attempt.node_id not in attempts_by_node:
            attempts_by_node[attempt.node_id] = []
        attempts_by_node[attempt.node_id].append(attempt)
        if attempt.status=="in_progress":
            in_progress = attempt

    periods = deepcopy(get('periods'))
    period_ready = True
    
    for _period_name, period in periods.items():
        prev_period_ready = period_ready
        for topic in period:
            topic_ready = True
            for node in topic['nodes']:
                attempts = attempts_by_node.get(node['id']) or []

                if len(attempts):
                    status = attempts[-1].status
                    if status == 'failed' or status == 'repair':
                        status = 'startable'
                else:
                    status = 'startable' if prev_period_ready and topic_ready and in_progress==None else 'future'
                    for dep_id in node['depend']:
                        dep_attempts = attempts_by_node.get(dep_id,[])
                        if not dep_attempts or dep_attempts[-1].status != 'passed':
                            status = 'future'

                if status != 'passed':
                    period_ready = False
                    topic_ready = False

                node['attempts'] = attempts
                node['status'] = status

            if topic_ready:
                topic['status'] = 'ready'
            elif prev_period_ready:
                topic['status'] = 'progress'

    return periods
            

def get_node_with_status(node_id, student):
    # TODO: this could and should be faster!
    periods = get_periods_with_status(student)
    pos = get('nodes_by_id')[node_id]['position']
    return periods[f"{pos[0]}.{pos[1]}"][pos[2]]['nodes'][pos[3]]