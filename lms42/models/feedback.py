from ..app import db, app
import datetime
import sqlalchemy
import json
import enum
import os
from .grading import Grading
from .user import User


class NodeFeedback(db.Model):
    node_id = db.Column(db.String, nullable=False)

    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    student = db.relationship("User", foreign_keys="NodeFeedback.student_id")

    __table_args__ = (
        db.PrimaryKeyConstraint(node_id,student_id),
    )

    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)

    assignment_clarity = db.Column(db.SmallInteger) # 1-5 (5 is optimal)
    difficulty = db.Column(db.SmallInteger) # 1-5 (3 is optimal)
    hours = db.Column(db.SmallInteger) # in hours
    fun = db.Column(db.SmallInteger) # 1-5 (5 is optimal)
    resource_quality = db.Column(db.SmallInteger) # 1-5 (5 is optimal)

    comments = db.Column(db.Text)


# TODO: MentorFeedback?
