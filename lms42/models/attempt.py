from ..app import db, app
import datetime
import sqlalchemy
import json
import enum
import os
import flask
from .grading import Grading
from .. import utils
from .user import User


FORMATIVE_ACTIONS = {
    "passed": "The student passes.",
    "repair": "The student needs to correct some work.",
    "failed": "The student needs to start anew.",
}


class Attempt(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    student_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    student = db.relationship("User", foreign_keys="Attempt.student_id", back_populates="attempts")

    number = db.Column(db.Integer, nullable=False)

    node_id = db.Column(db.String, nullable=False)
    variant_id = db.Column(db.SmallInteger, nullable=False)

    start_time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    deadline_time = db.Column(db.DateTime)
    upload_time = db.Column(db.DateTime)
    submit_time = db.Column(db.DateTime)

    status = db.Column(db.String, nullable=False) # awaiting_approval, in_progress, needs_grading, needs_consent, passed, repair, failed

    credits = db.Column(db.Integer, nullable=False, default=0)
    
    mentor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    mentor = db.relationship("User", foreign_keys="Attempt.mentor_id")

    gradings = db.relationship("Grading", lazy='dynamic')

    avg_days = db.Column(db.Float, nullable=False)

    @property
    def latest_grading(self):
        return self.gradings.order_by(Grading.id.desc()).first()

    @property
    def directory(self):
        return os.path.join('data', 'attempts', f"{self.node_id}-{self.student_id}-{self.number}")

    __table_args__ = (
        db.Index('attempt_status_index', 'status', 'student_id'),
        db.UniqueConstraint('student_id', 'node_id', 'number'),
    )


    def write_json(self):
        d = utils.model_to_dict(self)
        d["student"] = {
            "full_name": self.student.full_name,
            "email": self.student.email,
        }
        d["gradings"] = [utils.model_to_dict(grading) for grading in self.gradings]
        with open(os.path.join(self.directory, "attempt.json"), "w") as file:
            file.write(json.dumps(d,indent=4))


def get_notifications(attempt, grading):
    notifications = []

    if attempt.status == "in_progress":
        notifications.append("In progress!")

    if attempt.submit_time:
        notifications.append(f"Attempt by {attempt.student.full_name} from {utils.utc_to_display(attempt.start_time)} until {utils.utc_to_display(attempt.submit_time)}.")
    else:
        notifications.append(f"Attempt by {attempt.student.full_name} started {utils.utc_to_display(attempt.start_time)}.")

    if grading:
        notifications.append(f"Graded by {grading.grader.full_name} on {utils.utc_to_display(grading.time)}.")
        if attempt.status in FORMATIVE_ACTIONS:
            notifications.append(FORMATIVE_ACTIONS[attempt.status])
        if grading.grade_motivation:
            notifications.append(f"<i>{flask.escape(grading.grade_motivation)}</i>")

    if attempt.deadline_time:
        if attempt.status == "in_progress":
            notifications.append(f"Deadline: {utils.utc_to_display(attempt.deadline_time)}")

        end = attempt.submit_time or datetime.datetime.utcnow()
        if end > attempt.deadline_time:
            delta = end - attempt.deadline_time
            print(end, attempt.deadline_time)
            notifications.append(f"Deadline exceeded by {delta.seconds//60 + delta.days*8*60} minutes!")

    if attempt.mentor_id != None:
        notifications.append(f"Mentor: {attempt.mentor.full_name}.")

    return notifications
