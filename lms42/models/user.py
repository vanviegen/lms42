from ..app import db
from flask_login import current_user
import datetime
import os
import secrets
import base64
from sqlalchemy import orm
from pathlib import Path


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String, nullable=False)
    email = db.Column(db.String, nullable=False, unique=True)
    level = db.Column(db.SmallInteger, nullable=False, default=10)
    # 10 student
    # 30 inspector
    # 50 teacher
    # 80 admin
    # 90 owner

    short_name = db.Column(db.String, nullable=False, unique=True)
    @orm.validates('short_name')
    def convert_upper(self, key, value):
        return value.lower()


    counselor_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    counselees = db.relationship("User", backref=db.backref('counselor', remote_side=[id]))

    attempts = db.relationship("Attempt", lazy='dynamic', foreign_keys="Attempt.student_id", back_populates="student")

    current_attempt_id = db.Column(db.Integer, db.ForeignKey('attempt.id', use_alter=True))
    current_attempt = db.relationship("Attempt", foreign_keys="User.current_attempt_id")

    absent_days = db.Column(db.ARRAY(db.Integer), nullable=False, default=[])

    # The following properties and method are required by Flask Login:
    is_active = db.Column(db.Boolean, nullable=False, default=True)
    is_authenticated = True
    is_anonymous = False
    def get_id(self):
        return str(self.id)

    @property
    def is_fake(self):
        return "@" not in self.email

    @property
    def is_inspector(self):
        return self.level >= 30

    @property
    def is_teacher(self):
        return self.level >= 50

    @property
    def is_admin(self):
        return self.level >= 80
        
    @property
    def is_owner(self):
        return self.level >= 90

    avatar_name = db.Column(db.String)

    @property
    def avatar(self):
        if self.avatar_name:
            return f"/static/avatars/{self.avatar_name}"
        else:
            return "/static/placeholder.png"

    @avatar.setter
    def avatar(self, data_uri):
        # TODO: this is not transaction safe.
        if self.avatar_name:
            try:
                self.avatar_name = None
                os.unlink(os.path.join("data","avatars",self.avatar_name))
            except:
                pass

        if not data_uri:
            return

        comma = data_uri.find(';base64,')
        if comma:
            header = data_uri[0:comma]
            image = base64.b64decode(data_uri[comma+8:])
            ext = ".jpg" if header.find("/jpeg") else (".png" if header.find("/png") else None)
            if ext:
                self.avatar_name = secrets.token_urlsafe(8) + ext
                with open(os.path.join("data","avatars",self.avatar_name), "wb") as file:
                    file.write(image)
                return
        raise Exception(f"Couldn't parse avatar data url starting with {data_uri[0:40]}")


    @property
    def description(self):
        if self.is_teacher:
            return "Teacher"
        if self.is_inspector:
            return "Inspector"
        if self.current_attempt_id:
            return curriculum.get('nodes_by_id')[self.current_attempt.node_id]['name']
        return 'Idle student'

    @property
    def full_name(self):
        return (self.first_name + " " + self.last_name).strip()

    @property
    def url_query(self):
        if current_user.id != self.id:
            return f"?student={self.id}"
        else:
            return ''

    class_name = db.Column(db.String, nullable=False, default='')

    # Just to make REPL work more pleasant:
    def __repr__(self):
        return '<User {}: {}>'.format(self.id, self.short_name)


class LoginLink(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.datetime.utcnow, nullable=False)
    secret = db.Column(db.String, nullable=False)
    
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user = db.relationship('User')

    redirect_url = db.Column(db.String)


class TimeLog(db.Model):
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False, primary_key=True)
    user = db.relationship('User')

    date = db.Column(db.Date, nullable=False, primary_key=True)

    start_time = db.Column(db.Time, nullable=False)
    end_time = db.Column(db.Time, nullable=False)


from ..models import curriculum
