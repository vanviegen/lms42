import datetime
from collections import defaultdict


VACATIONS = [
    # 2020/2021 (starting February)
    ('2021-02-22', '2021-02-26'),
    '2021-04-02',
    '2021-04-05',
    '2021-04-27',
    ('2021-05-03', '2021-05-07'),
    '2021-05-13',
    '2021-05-14',
    '2021-05-24',
    ('2021-07-19', '2021-08-27'),
    ('2021-08-30', '2021-08-31'), # intro days

    # 2021/2022
    ('2021-10-18', '2021-10-22'),
    ('2021-12-27', '2022-01-07'),
    ('2022-02-21', '2022-02-25'),
    '2022-04-15',
    '2022-04-18',
    '2022-04-27',
    ('2022-05-02', '2022-05-06'),
    '2022-05-26',
    '2022-05-27',
    '2022-06-06',
    ('2022-07-18', '2022-08-26'),
    ('2021-08-29', '2021-08-30'), # intro days
]


# Precalculate a set containing all vacation days
VACATION_DAYS = set()
for item in VACATIONS:
    if isinstance(item, str):
        VACATION_DAYS.add(datetime.date.fromisoformat(item))
    else:
        date = datetime.date.fromisoformat(item[0])
        end_date = datetime.date.fromisoformat(item[1])
        while date <= end_date:
            VACATION_DAYS.add(date)
            date += datetime.timedelta(days=1)


def is_working_day(date: datetime.date):
    return date.weekday() not in [5,6] and date not in VACATION_DAYS


def offset(date: datetime.date, days: int):
    for _ in range(days):
        date += datetime.timedelta(days=1)
        while not is_working_day(date):
            date += datetime.timedelta(days=1)
    return date


def calculate_per_month(date: datetime.date, end_date: datetime.date = datetime.date.today()):
    result = defaultdict(int)
    while date < end_date:
        if is_working_day(date):
            result[f"{date.year}-{date.month:02}"] += 1
        date += datetime.timedelta(days=1)
    return dict(result)

