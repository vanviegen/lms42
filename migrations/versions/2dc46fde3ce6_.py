"""empty message

Revision ID: 2dc46fde3ce6
Revises: b0f9f0b6ab77
Create Date: 2020-10-23 09:58:20.120768

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2dc46fde3ce6'
down_revision = 'b0f9f0b6ab77'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('user', sa.Column('level', sa.SmallInteger()))
    op.execute('update "user" set level = case when is_teacher then 50 else 10 end')
    op.alter_column('user', 'level', nullable=False)
    op.drop_column('user', 'is_teacher')

    op.add_column('attempt', sa.Column('deadline_time', sa.DateTime(), nullable=True))
    op.add_column('attempt', sa.Column('grade_details_json', sa.String(), nullable=True))
    op.add_column('attempt', sa.Column('submit_time', sa.DateTime(), nullable=True))
    op.add_column('attempt', sa.Column('upload_time', sa.DateTime(), nullable=True))
    op.drop_column('attempt', 'end_time')

    op.add_column('attempt', sa.Column('number', sa.Integer(), nullable=False))

    op.drop_column('attempt', 'lesson_version')
    op.add_column('attempt', sa.Column('node_id', sa.String(), nullable=False))
    op.drop_column('attempt', 'lesson_id')

    op.create_unique_constraint(op.f('uq_attempt_student_id'), 'attempt', ['student_id', 'node_id', 'number'])

    op.add_column('attempt', sa.Column('variant_id', sa.SmallInteger(), nullable=False))
    op.drop_column('user', 'avatar_data')

    op.add_column('user', sa.Column('current_attempt_id', sa.Integer(), nullable=True))

    op.create_foreign_key(op.f('fk_user_current_attempt_id_attempt'), 'user', 'attempt', ['current_attempt_id'], ['id'], use_alter=True)
    op.drop_constraint('uq_attempt_student_id', 'attempt', type_='unique')


def downgrade():
    op.add_column('user', sa.Column('is_teacher', sa.BOOLEAN(), autoincrement=False, nullable=False, server_default="true"))
    op.drop_column('user', 'level')

    op.add_column('attempt', sa.Column('end_time', postgresql.TIMESTAMP(), autoincrement=False, nullable=True))
    op.drop_column('attempt', 'upload_time')
    op.drop_column('attempt', 'submit_time')
    op.drop_column('attempt', 'grade_details_json')
    op.drop_column('attempt', 'deadline_time')

    op.create_index('attempt_student_index', 'attempt', ['student_id', 'lesson_id', 'start_time'], unique=False)
    op.drop_constraint(op.f('uq_attempt_student_id'), 'attempt', type_='unique')
    op.drop_column('attempt', 'number')

    op.add_column('attempt', sa.Column('lesson_version', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.alter_column('attempt', 'deadline_time',
               existing_type=postgresql.TIMESTAMP(),
               nullable=False)

    op.add_column('user', sa.Column('avatar_data', sa.VARCHAR(), autoincrement=False, nullable=True))
    op.drop_column('attempt', 'variant_id')

    op.drop_constraint(op.f('fk_user_current_attempt_id_attempt'), 'user', type_='foreignkey')
    op.drop_column('user', 'current_attempt_id')
    
    op.add_column('attempt', sa.Column('lesson_id', sa.VARCHAR(), autoincrement=False, nullable=False))
    op.drop_constraint(op.f('uq_attempt_student_id'), 'attempt', type_='unique')
    op.create_unique_constraint('uq_attempt_student_id', 'attempt', ['student_id', 'lesson_id', 'number'])
    op.drop_column('attempt', 'node_id')

